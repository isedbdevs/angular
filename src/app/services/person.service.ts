import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/map';
import { SharedService } from "./shared.service";
import { ClubService } from "./club.service";

@Injectable()
export class PersonService {
  Persons: EventEmitter<any> = new EventEmitter<any>();
  updateStatus: EventEmitter<any> = new EventEmitter<any>();

  /**
   * true is emitted when the client should retrieve the updated list of SharedService.AllPersonInTournament
   * @type {EventEmitter<any>}
   */
  retrieveAllPersonInTournament: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient, private sharedServ: SharedService, private clubServ: ClubService) {
  }

  /**
   * Gets all people from the back-end
   */
  getPersons() {
    const req = this.http.get(this.sharedServ.SERVER_URL + '/person/').subscribe((success) => {
        this.sharedServ.allPersons = [];
        JSON.parse(JSON.stringify(success)).forEach((row) => {
          this.sharedServ.allPersons.push({
            address: row.address,
            clubId: '' + row.clubId,
            elo: '' + row.elo,
            email: row.email,
            firstname: row.firstname,
            gender: row.gender,
            lastname: row.lastname,
            personId: '' + row.personId,
            phonenumber: row.phonenumber,
            place: row.place,
            prefix: row.prefix,
            zipcode: row.zipcode
          });
        });
        this.Persons.emit(success);
        if (this.sharedServ.updatePersonsForClub) {
          this.clubServ.getClubs();
          this.sharedServ.updatePersonsForClub = false;
        }
      },
      (error) => {
        return 'error';
      });
  }

  /**
   * Deletes a person
   * @param {string} personid id of person that's going to be deleted
   * @returns {Promise<string>}
   */
  async deletePerson(personid: string): Promise<string> {
    this.http.delete(this.sharedServ.SERVER_URL + '/person/' + personid).subscribe((succes) => {
      this.getPersons();
      return 'success';
    }, (error) => {
      this.getPersons();
      if (error.error.error === 'Not Found') {
        return 'success';
      }
      else {
        return 'error';
      }
    });
    return 'success';
  }

  /**
   * Creates new person
   * @param person object which holds the values for the new person
   */
  addPerson(person: any) {
    this.http.post(this.sharedServ.SERVER_URL + '/person/', {
      firstname: person.firstname,
      lastname: person.lastname,
      prefix: person.prefix,
      clubId: person.clubId,
      gender: person.gender,
      address: person.address,
      place: person.place,
      zipcode: person.zipcode,
      email: person.email,
      phonenumber: person.phonenumber
    }).subscribe((success) => {
      this.getPersons();
    }, (error) => {

    });
  }

  /**
   * Changes a person
   * @param person has values of the modified person
   */
  updatePerson(person: any) {
    this.http.put(this.sharedServ.SERVER_URL + '/person/' + person.personId, {
      firstname: person.firstname,
      lastname: person.lastname,
      prefix: person.prefix,
      clubId: person.clubId,
      gender: person.gender,
      address: person.address,
      place: person.place,
      zipcode: person.zipcode,
      email: person.email,
      phonenumber: person.phonenumber,
      personId: 0
    }).subscribe((success) => {
      this.updateStatus.emit(success);
    }, (error) => {
      this.updateStatus.emit('error');
    });
  }

  /**
   * Changes the club of a person
   * @param person
   * @param clubId new Club person is registered to
   */
  updatePersonClub(person: any, clubId) {
    this.http.put(this.sharedServ.SERVER_URL + '/person/' + person.personId, {
      firstname: person.firstname,
      lastname: person.lastname,
      prefix: person.prefix,
      clubId: clubId,
      gender: person.gender,
      address: person.address,
      place: person.place,
      zipcode: person.zipcode,
      email: person.email,
      phonenumber: person.phonenumber,
      personId: 0
    }).subscribe((success) => {
      this.updateStatus.emit('success');
    }, (error) => {
      this.updateStatus.emit('error');
    });
  }

  /**
   * Removing a person from any club they are in
   * @param person
   */
  deletePersonFromClub(person: any) {
    this.http.put(this.sharedServ.SERVER_URL + '/person/' + person.personId, {
      firstname: person.firstname,
      lastname: person.lastname,
      prefix: person.prefix,
      clubId: null,
      gender: person.gender,
      address: person.address,
      place: person.place,
      zipcode: person.zipcode,
      email: person.email,
      phonenumber: person.phonenumber,
      personId: 0
    }).subscribe((success) => {
      this.updateStatus.emit('success');
      this.getPersons();
      this.clubServ.getClubs();
    }, (error) => {
      this.updateStatus.emit('error');
    });
  }

  /**
   * Gets all people in tournaments
   */
  getPersonInTournament() {
    this.sharedServ.allPersonInTournament = [];
    this.http.get(this.sharedServ.SERVER_URL + '/persontournament/').subscribe((success) => {
      JSON.parse(JSON.stringify(success)).forEach((row) => {
        this.sharedServ.allPersonInTournament.push({
          personId: row.personId + '',
          tournamentId: row.tournamentId + ''
        });
      });
      this.retrieveAllPersonInTournament.emit(true);
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * Deletes a person from a tournament
   * @param person
   * @param tournament
   */
  deletePersonFromTournament(person: any, tournament) {
    this.http.request('delete', this.sharedServ.SERVER_URL + '/persontournament/', {
      body: {
        tournamentId: tournament.tournamentId,
        personId: person.personId
      }
    }).subscribe(() => {
      this.getPersonInTournament();
    }, (error) => {
      if (error.error.error === "Not Found") {
        this.getPersonInTournament();
      }
    });
  }

  updatePersonTournament(person: any, tournamentId: any) {
    this.http.post(this.sharedServ.SERVER_URL + '/persontournament/', {
      personId: person.personId,
      tournamentId: tournamentId
    }).subscribe(() => {
      this.getPersonInTournament();
    });
  }
}
