import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { SharedService } from "./shared.service";

@Injectable()
export class ClubService {
  Clubs: EventEmitter<any> = new EventEmitter<any>();
  Club: EventEmitter<any> = new EventEmitter<any>();
  updateStatus: EventEmitter<any> = new EventEmitter<any>();
  getPersons: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient, private sharedServ: SharedService) {
  }

  /**
   * Gets all clubs from the back-end and saves it in SharedService.allClubs
   */
  getClubs() {
    this.http.get(this.sharedServ.SERVER_URL + '/club/').subscribe((success) => {
        this.sharedServ.allClubs = [];
        this.Clubs.emit(success);
        JSON.parse(JSON.stringify(success)).forEach((row) => {
          this.sharedServ.allClubs.push({
            clubId: row.clubId + '',
            name: row.name,
            clubStreetAddress: row.clubStreetAddress,
            clubPlace: row.clubPlace
          });
        });
        this.getPersons.emit(true);
      },
      (error) => {
        console.log(error);
        return 'error';
      });
  }

  /**
   * Deletes a club and gets an updated list of clubs
   * @param {string} personid
   * @returns {Promise<string>}
   */
  async deleteClub(personid: string): Promise<string> {
    this.http.delete(this.sharedServ.SERVER_URL + '/club/' + personid).subscribe((succes) => {
      this.getClubs();
      return 'success';
    }, (error) => {
      if (error.error.error === 'Not Found') {
        this.getClubs();
        return 'success';
      }
      else {
        return 'error';
      }
    });
    return 'success';
  }

  /**
   * Creates a new club and gets updated list of players from the server
   * @param club holds data of the new club
   */
  addClub(club: any) {
    this.http.post(this.sharedServ.SERVER_URL + '/club/', {
      clubPlace: club.clubPlace,
      clubStreetAddress: club.clubStreetAddress,
      name: club.name
    }).subscribe((success) => {
      this.getClubs();
    }, (error) => {

    });
  }

  /**
   * Changes a club
   * @param club holds changed data of the club
   */
  updateClub(club: any) {
    this.http.put(this.sharedServ.SERVER_URL + '/club/' + club.clubId, {
      clubPlace: club.clubPlace,
      clubStreetAddress: club.clubStreetAddress,
      name: club.name,
      clubId: 0
    }).subscribe((success) => {
      this.updateStatus.emit('success');
    }, (error) => {
      this.updateStatus.emit('error');
    });
  }

  /**
   * Gets a specific Club and emits the received data to Club
   * @param clubId
   */
  getClub(clubId) {
    this.http.get(this.sharedServ.SERVER_URL + '/club/' + clubId).subscribe((success) => {
      this.Club.emit(success);
    });
  }
}
