import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
  /**
   * All HTTP requests use this url as base url
   * @type {string} base url of server
   */
  SERVER_URL: string = 'http://localhost:8080';
  updatePersonsForClub: boolean = false;
  /**
   * Array that contains all clubs
   * @type {any[]}
   */
  allClubs: {
    clubId: string
    name: string
    clubStreetAddress: string
    clubPlace: string
  }[] = [];

  /**
   * Array that contains all people
   * @type {any[]}
   */
  allPersons: {
    address?: string,
    clubId?: string,
    elo?: string,
    email?: string,
    firstname?: string,
    gender?: string,
    lastname?: string,
    personId?: string,
    phonenumber?: string,
    place?: string,
    prefix?: string,
    zipcode?: string,
    clubName?: string
  }[] = [];

  /**
   * Array that contains all tournaments
   * @type {any[]}
   */
  allTournaments: {
    tournamentId: string,
    tournamentName: string,
    tournamentDate: Date,
    roundAmount: string
  }[] = [];

  /**
   * Array that contains all relations between people and tournaments
   * @type {any[]}
   */
  allPersonInTournament: {
    personId: string,
    tournamentId: string
  }[] = [];
  updatePersonsForTournament: boolean;

  /**
   * Object that contains all data of the selected tournament in TournamentsComponent
   */
  selectedTournament: {
    tournamentId?: string,
    tournamentName?: string,
    tournamentDate?: any,
    roundAmount?: string
    persons?: any[]
  };

  /**
   * Contains all people that are in a tournament and their scores
   * @type {any[]}
   */
  tournamentPersonScores: {
    tournamentId: string, scores?: {
      personId?: string,
      score?: string
    }[]
  }[] = [];

  constructor() {
  }

}
