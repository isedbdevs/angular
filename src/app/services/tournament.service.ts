import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { SharedService } from "./shared.service";

@Injectable()
export class TournamentService {
  tournaments: EventEmitter<any> = new EventEmitter<any>();
  updateStatus: EventEmitter<any> = new EventEmitter<any>();
  retrieveTournaments: EventEmitter<any> = new EventEmitter<any>();
  retrievePersonScores: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient, private sharedServ: SharedService) {
  }

  /**
   * Gets all tournaments from the back-end and pushes it to SharedService.allTournaments
   */
  getTournaments() {
    this.http.get(this.sharedServ.SERVER_URL + '/tournament/').subscribe((success) => {
        this.sharedServ.allTournaments = [];
        this.tournaments.emit(success);
        JSON.parse(JSON.stringify(success)).forEach((row) => {
          this.sharedServ.allTournaments.push({
            tournamentId: row.tournamentId + '',
            tournamentName: row.tournamentName,
            tournamentDate: new Date(row.tournamentDate),
            roundAmount: row.roundAmount
          });
        });
        this.retrieveTournaments.emit(true);
      },
      (error) => {
        console.log(error);
        return 'error';
      });
  }

  /**
   * Creates a new tournament
   * @param {{ @param {string} tournamentName name of new tournament @param {string} tournamentDate date of new tournament @param {string} roundAmount amount of rounds the new tournament has } newTournament
   // * @param { tournamentName tournamentDate roundAmount } newTournament
   */
  addTournament(newTournament: { tournamentName: string; tournamentDate: string; roundAmount: string }) {
    this.http.post(this.sharedServ.SERVER_URL + '/tournament/', {
      tournamentName: newTournament.tournamentName,
      tournamentDate: newTournament.tournamentDate,
      roundAmount: newTournament.roundAmount
    }).subscribe(() => {
      this.getTournaments();
    });
  }

  /**
   * Deletes a tournament
   * @param {string} tournamentId
   */
  deleteTournament(tournamentId: string) {
    this.http.delete(this.sharedServ.SERVER_URL + '/tournament/' + tournamentId).subscribe(() => {
      this.getTournaments();
    }, (error) => {
      if (error.error.error === "Not Found") {
        this.getTournaments();
      }
    })
  }

  /**
   * Gets scores of people in a specific tournament
   * @param {string} tournamentId
   */
  getTournamentScores(tournamentId: string) {
    try {
      this.sharedServ.tournamentPersonScores.find((obj) => {
        return obj.tournamentId === tournamentId;
      }).scores = [];
    }
    catch {

    }
    this.http.get(this.sharedServ.SERVER_URL + '/grouppersonscore/' + tournamentId).subscribe((success) => {
      JSON.parse(JSON.stringify(success)).forEach((row) => {
        let entry = this.sharedServ.tournamentPersonScores.find((o1) => {
          return +o1.tournamentId === +tournamentId;
        });
        if (entry === undefined) {
          this.sharedServ.tournamentPersonScores.push({
            tournamentId: tournamentId,
            scores: []
          });
          entry = this.sharedServ.tournamentPersonScores.find((o1) => {
            return (o1.tournamentId + '') === (tournamentId + '')
          });
        }
        if (entry !== undefined) {
          if (entry.scores.find((object) => {
            return (+object.personId === +row.personId)
          }) === undefined) {
            entry.scores.push({
              personId: '' + row.personId,
              score: 0 + ''
            })
          }
          let perss = entry.scores.find((object) => {
            return (+object.personId == +row.personId)
          });
          if (perss.score == undefined) {
            perss.score = 0 + '';
          }
          perss.score = (+perss.score + +row.groupPersonScore) + '';
        }
      });
      this.retrievePersonScores.emit(true);
    }, (error) => {
      console.log(error);
    });
  }
}
