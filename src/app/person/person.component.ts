import { Component, OnInit } from '@angular/core';
import { NamePipe } from "../name.pipe";
import { PersonService } from "../services/person.service";
import { Subscription } from "rxjs/Subscription";
import { SharedService } from "../services/shared.service";
import { ClubService } from "../services/club.service";

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  /**
   * Used to display the loading bar
   * @type {boolean}
   */
  loading: boolean = true;

  /**
   * Stores subscriptions
   * @type {Subscription}
   */
  status: Subscription = new Subscription();

  /**
   * Displays the create new person form
   * @type {boolean}
   */
  addMode: boolean = false;

  /**
   * displays the edit person form
   * @type {boolean}
   */
  editMode: boolean = false;

  /**
   * Has data when a user clicks on a person from the person list
   */
  selectedPerson: {
    personId?: string,
    firstname?: string,
    lastname?: string,
    email?: string,
    prefix?: string,
    gender?: string,
    address?: string,
    zipcode?: string,
    phonenumber?: string,
    clubId?: string,
    clubName?: string
  };

  /**
   * Array of all people that are displayed in the Person list
   * This array is filtered when the user types something in the search field
   * @type {any[]}
   */
  searchResults: {
    address?: string,
    clubId?: string,
    elo?: string,
    email?: string,
    firstname?: string,
    gender?: string,
    lastname?: string,
    personId?: string,
    phonenumber?: string,
    place?: string,
    prefix?: string,
    zipcode?: string
  }[] = [];

  /**
   * Object used when creating a new user
   * @type {{firstname: string; lastname: string; email: string; clubId: string}}
   */
  newPerson: {
    firstname: string,
    lastname: string,
    email?: string,
    clubId: string,
    prefix?: string,
    gender?: string,
    address?: string,
    zipcode?: string,
    phonenumber?: string,
    place?: string
  } = {
    firstname: '',
    lastname: '',
    email: '',
    clubId: 'null'
  };

  /**
   * Contains a list of all clubs
   */
  clubs: any;

  /**
   * Search query which is linked to the search input field
   * @type {string}
   */
  search: string = '';

  /**
   * Loading bar which is displayed when creating a new person
   * @type {boolean}
   */
  loadingadd: boolean = false;

  /**
   * Loading bar which is displayed when editing a person
   * @type {boolean}
   */
  loadingedit: boolean = false;

  /**
   * Holds the javascript click event when selecting a person
   */
  selectPersonEvent: any;

  constructor(private personServ: PersonService, private name: NamePipe, public sharedServ: SharedService, private clubServ: ClubService) {
  }

  ngOnInit() {
    if (this.sharedServ.allClubs.length <= 0) {
      this.clubServ.getClubs();
    }
    this.subscribeToPersons();
    if (this.sharedServ.allClubs.length <= 0) {
      this.clubServ.getClubs();
    }
  }

  /**
   * Selects a person from the person list
   * @param event javascript click event
   * @param person object of selected person
   */
  selectPerson(event: any, person: any) {
    this.selectPersonEvent = event;
    this.cancelAllModes();
    let selectedItems = document.getElementById('person-list');
    for (let i = 0; i < selectedItems.children.length; i++) {
      selectedItems.children[i].classList.toggle('person-item-selected', false);
    }
    event.target.classList.add('person-item-selected');
    this.selectedPerson = person;
    if (this.sharedServ.allClubs.length > 0) {
      if (person.clubId != null && person.clubId != undefined && person.clubId != 'null') {
        person.clubName = this.sharedServ.allClubs.find((club) => {
          return club.clubId === person.clubId;
        }).name;
      }
    }
  }

  /**
   * Shows the delete person popup
   */
  deletePerson() {
    document.getElementById('delete-popup').classList.toggle('hidden', false);
  }

  /**
   * Hides the delete person popup
   */
  disablePopup() {
    document.getElementById('delete-popup').classList.toggle('hidden', true);
  }

  /**
   * Enables add mode
   * Add mode shows the create new person form
   */
  enableAddMode() {
    this.editMode = false;
    this.addMode = true;
    this.newPerson = {
      firstname: '',
      lastname: '',
      email: '',
      clubId: 'null'
    };
  }

  /**
   * Enables edit mode
   * Shows the form to edit a person
   */
  enableEditMode() {
    if (this.selectedPerson != null || this.selectedPerson != undefined) {
      this.addMode = false;
      this.newPerson = {
        firstname: '',
        lastname: '',
        email: '',
        clubId: 'null'
      };
      this.editMode = true;
      this.clubs = this.sharedServ.allClubs;
      setTimeout(() => {
        this.isdirty();
      }, 50);
    }
    else {
      this.deletePerson();
    }
  }

  /**
   * disabled edit mode as well as add mode and resets the object used to create a new person
   */
  cancelAllModes() {
    this.editMode = false;
    this.addMode = false;
    this.newPerson = {
      firstname: '',
      lastname: '',
      email: '',
      clubId: 'null'
    };
  }

  /**
   * Sends necessary data over to PersonServ to create a new person and then gets the new list of people
   */
  createNewPerson() {
    this.personServ.addPerson(this.newPerson);
    this.loadingadd = true;
    this.personServ.getPersons();
  }

  /**
   * Deletes a person and when person is succesfully deleted it gets the updated list of persons
   * @param {string} person_id
   */
  confirmDelete(person_id: string) {
    this.personServ.deletePerson(person_id).then((status) => {
      if (status === 'success') {
        this.personServ.getPersons();
      }
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * Filters searchResults based on the search string
   */
  searchChange() {
    this.searchResults = [];
    this.sharedServ.allPersons.forEach((person) => {
      let name: string = this.name.transform(person);
      if (name.toLowerCase().includes(this.search.toLowerCase())) {
        this.searchResults.push(person);
      }
    });
    if (this.search === '') {
      this.searchResults = this.sharedServ.allPersons;
    }
    this.filterPersons();
  }

  /**
   * Adds necessary MDL classes when the input fields are in dirty state
   */
  isdirty() {
    if (this.selectedPerson.firstname !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('firstname-edit').parentElement.classList.add('is-dirty');
      document.getElementById('firstname-edit').parentElement.classList.remove('is-invalid');
    }
    if (this.selectedPerson.prefix !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('prefix-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedPerson.lastname !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('lastname-edit').parentElement.classList.add('is-dirty');
      document.getElementById('lastname-edit').parentElement.classList.remove('is-invalid');
    }
    if (this.selectedPerson.email !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('email-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedPerson.address !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('address-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedPerson.zipcode !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('zipcode-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedPerson.phonenumber !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('phonenumber-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedPerson.gender !== null && this.selectedPerson.phonenumber !== undefined) {
      document.getElementById('gender-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedPerson.clubId !== null && this.selectedPerson.clubId !== undefined) {
      document.getElementById('clubId-edit').parentElement.classList.add('is-dirty');
    }
  }

  /**
   * Edits a person
   */
  editPerson() {
    this.loadingedit = true;
    this.status = new Subscription();
    if (this.status !== null) {
      this.status.add(this.personServ.updateStatus.subscribe((success) => {
        this.selectedPerson = success;
        success.clubId = success.clubId + '';
        this.selectPerson(this.selectPersonEvent, success);
        this.loadingedit = false;
        this.cancelAllModes();
        this.resetEdit();
      }, (error) => {
        console.log(error);
        this.resetEdit();
        this.loadingedit = false;
      }));
    }
    setTimeout(() => {
      this.personServ.updatePerson(this.selectedPerson);
    }, 10);
  }

  /**
   * Unsubscribed from event
   * @returns {Promise<void>}
   */
  async resetEdit() {
    setTimeout(() => {
      this.status.unsubscribe();
      this.status = null;
    }, 10);
  }

  /**
   * Sorts searchResults by lastname descending
   */
  filterPersons() {
    this.searchResults = this.searchResults.sort((person1, person2) => {
      if (person1.lastname < person2.lastname) return -1;
      else if (person1.lastname > person2.lastname) return 1;
      else return 0;
    });
  }

  /**
   * Validator for create new person input fields
   * @returns {boolean} return true when allowed to proceed
   */
  mayCreateNewPerson(): boolean {
    return ((this.newPerson.firstname !== '' && this.newPerson.firstname !== undefined && !(new RegExp('^[\\s]+?$').test(this.newPerson.firstname))) && (this.newPerson.lastname !== '' && this.newPerson.lastname !== undefined && !(new RegExp('^[\\s]+?$').test(this.newPerson.lastname))));
  }

  /**
   * Validator for editing person input fields
   * @returns {boolean} returns true when allowed to proceed
   */
  mayEditPerson(): boolean {
    return ((this.selectedPerson.firstname !== '' && this.selectedPerson.firstname !== undefined && !(new RegExp('^[\\s]+?$').test(this.selectedPerson.firstname))) && (this.selectedPerson.lastname !== '' && this.selectedPerson.lastname !== undefined && !(new RegExp('^[\\s]+?$').test(this.selectedPerson.lastname))));
  }

  /**
   * Subscribed to PersonService.Persons
   */
  private subscribeToPersons() {
    const subscription = this.personServ.Persons.subscribe((data) => {
      this.searchResults = this.sharedServ.allPersons;
      this.filterPersons();
      this.loadingadd = false;
      this.editMode = false;
      this.addMode = false;
      this.newPerson = {
        firstname: '',
        lastname: '',
        email: '',
        clubId: 'null'
      };
      this.loading = false;
    }, () => {
      this.editMode = false;
      this.addMode = false;
      this.newPerson = {
        firstname: '',
        lastname: '',
        email: '',
        clubId: 'null'
      };
      this.loading = false;
      this.loadingadd = false;
    });
    this.loading = true;
    setTimeout(() => {
      if (this.sharedServ.allPersons.length <= 0) {
        this.personServ.getPersons();
      } else {
        this.personServ.Persons.emit(this.sharedServ.allPersons);
      }
      this.status.add(subscription);
    }, 50);
  }
}
