import { Component, OnInit } from '@angular/core';
import { ClubService } from "../services/club.service";
import { Subscription } from "rxjs/Subscription";
import { SharedService } from "../services/shared.service";
import { PersonService } from "../services/person.service";
import { NamePipe } from "../name.pipe";

@Component({
  selector: 'app-club',
  templateUrl: './club.component.html',
  styleUrls: ['./club.component.scss']
})
export class ClubComponent implements OnInit {
  /**
   * Object that contains the selected club from the club list
   */
  selectedClub: {
    clubId?: string
    name?: string
    clubStreetAddress?: string
    clubPlace?: string,
    persons?: any[]
  };

  /**
   * Displays the create new person form
   * @type {boolean}
   */
  addMode: boolean = false;

  /**
   * displays the edit person form
   * @type {boolean}
   */
  editMode: boolean = false;

  /**
   * Array of all clubs that are displayed in the club list
   * This array is filtered when the user types something in the search field
   * @type {any[]}
   */
  searchResults: {
    clubId: string
    name: string
    clubStreetAddress: string
    clubPlace: string
  }[] = [];

  /**
   * Object that contains data for the new club
   * @type {{name: string; clubStreetAddress: string; clubPlace: string}}
   */
  newClub: {
    name: string
    clubStreetAddress: string
    clubPlace: string
  } = {
    name: '',
    clubStreetAddress: '',
    clubPlace: ''
  };

  selectedPerson: any;
  /**
   * Holds the javascript click event when selecting a club
   */
  selectedClubEvent: any = null;

  /**
   * Search query which is linked to the search input field
   * @type {string}
   */
  search: string = '';

  /**
   * Used to display the loading bar
   * @type {boolean}
   */
  loading: boolean = false;
  /**
   * Loading bar which is displayed when creating a new club
   * @type {boolean}
   */
  loadingadd: boolean = false;
  /**
   * Loading bar which is displayed when editing a club
   * @type {boolean}
   */
  loadingedit: boolean = false;
  status: Subscription;

  allPersons: any = [];
  searchPersons: any = [];
  searchQueryPerson: string = '';
  selectedPersons: any = [];
  loadingAddPersons: boolean = false;
  selectedAmountOfPersons: number = 0;

  constructor(private clubServ: ClubService, private sharedServ: SharedService, private personServ: PersonService, private name: NamePipe) {
  }

  ngOnInit() {
    this.loading = true;
    if (this.sharedServ.allPersons.length <= 0) {
      this.personServ.getPersons();
    }
    this.subscribeToClubs();
    if (this.sharedServ.allClubs.length <= 0) {
      this.clubServ.getClubs();
    }
    else {
      this.clubServ.Clubs.emit(this.sharedServ.allClubs);
      this.filterClubs();
    }

  }

  /**
   * Selects a club from the club list
   * @param event javascript click event
   * @param club object of selected club
   */
  selectClub(event: any, club: any) {
    this.selectedClubEvent = event;
    this.cancelAllModes();
    let selectedItems = document.getElementById('person-list');
    for (let i = 0; i < selectedItems.children.length; i++) {
      selectedItems.children[i].classList.toggle('person-item-selected', false);
    }
    event.target.classList.add('person-item-selected');
    this.selectedClub = club;
    this.selectedClub.persons = [];
    this.sharedServ.allPersons.forEach((person) => {
      if (person.clubId === this.selectedClub.clubId) {
        this.selectedClub.persons.push({
          personId: person.personId,
          firstname: person.firstname,
          lastname: person.lastname,
          email: person.email,
          prefix: person.prefix,
          gender: person.gender,
          address: person.address,
          zipcode: person.zipcode,
          phonenumber: person.phonenumber,
          clubId: person.clubId
        });
      }
    });
  }

  /**
   * Shows delete club popup
   */
  deleteClub() {
    document.getElementById('delete-popup').classList.toggle('hidden', false);
  }

  /**
   * Hides all popups
   */
  disablePopup() {
    document.getElementById('delete-popup').classList.toggle('hidden', true);
    document.getElementById('add-person-popup').classList.toggle('hidden', true);
  }

  /**
   * Enables add mode
   * Add mode shows the create new person form
   */
  enableAddMode() {
    this.editMode = false;
    this.addMode = true;
  }

  /**
   * Enables edit mode
   * Shows the form to edit a person
   */
  enableEditMode() {
    if (this.selectedClub != null || this.selectedClub != undefined) {
      this.addMode = false;
      this.editMode = true;
      setTimeout(() => {
        this.isdirty();
      }, 50);
    }
    else {
      this.deleteClub();
    }
  }

  /**
   * disabled edit mode as well as add mode and resets the object used to create a new person
   */
  cancelAllModes() {
    this.editMode = false;
    this.addMode = false;
  }

  /**
   * Creates a new club
   */
  createNewClub() {
    this.clubServ.addClub(this.newClub);
  }

  /**
   * Deletes a club
   * @param {string} clubId if of the club
   */
  confirmDelete(clubId: string) {
    this.clubServ.deleteClub(clubId).then((status) => {
      if (status === 'success') {
        this.clubServ.getClubs();
      }
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * Detects changes in the search string and displays search results accordingly
   */
  searchChange() {
    this.searchResults = [];
    this.sharedServ.allClubs.forEach((club) => {
      let name: string = club.name;
      if (name.toLowerCase().includes(this.search.toLowerCase())) {
        this.searchResults.push(club);
      }
    });
    if (this.search === '') {
      this.searchResults = this.sharedServ.allClubs;
    }
    this.filterClubs();
  }

  /**
   * Adds necessary MDL classes when the input fields are in dirty state
   */
  isdirty() {
    if (this.selectedClub.name !== null && this.selectedClub.name !== undefined) {
      document.getElementById('name-edit').parentElement.classList.add('is-dirty');
      document.getElementById('name-edit').parentElement.classList.remove('is-invalid');
    }
    if (this.selectedClub.clubPlace !== null && this.selectedClub.clubPlace !== undefined) {
      document.getElementById('clubPlace-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedClub.clubStreetAddress !== null && this.selectedClub.clubStreetAddress !== undefined) {
      document.getElementById('clubStreetAddress-edit').parentElement.classList.add('is-dirty');
    }
  }

  /**
   * Edits a club
   */
  updateClub() {
    this.loadingedit = true;
    if (!(this.status)) {
      this.status = this.clubServ.updateStatus.subscribe((success) => {
        this.editMode = false;
        this.addMode = false;
        this.newClub = {
          name: '',
          clubStreetAddress: '',
          clubPlace: ''
        };
        this.resetEdit();
        this.loadingedit = false;
      }, (error) => {
        console.log(error);
        this.resetEdit();
        this.loadingedit = false;
      });
    }
    this.clubServ.updateClub(this.selectedClub);
  }

  /**
   * Resets edit subscription
   * @returns {Promise<void>}
   */
  async resetEdit() {
    setTimeout(() => {
      this.status.unsubscribe();
      this.status = null;
    }, 5);
  }

  /**
   * Deletes person from club
   * @param {{clubId?: string; name?: string; clubStreetAddress?: string; clubPlace?: string; persons?: {personObject: any}[]}} selectedClub
   */
  deletePersonFromClub(selectedClub: { clubId?: string; name?: string; clubStreetAddress?: string; clubPlace?: string; persons?: { personObject: any }[] }) {
    if(this.selectedPerson !== undefined && this.selectedPerson !== null) {
      if (this.selectedPerson.clubId === selectedClub.clubId) {
        this.personServ.deletePersonFromClub(this.selectedPerson);
      }
    }
  }

  /**
   * Displays add person to club popup
   */
  openPersonPopup() {
    this.allPersons = [];
    this.searchPersons = [];
    this.searchQueryPerson = '';
    this.selectedPersons = [];
    this.loadingAddPersons = false;
    this.resetAllPersons();
    for (var i = this.allPersons.length - 1; i >= 0; i--) {
      for (var j = 0; j < this.selectedClub.persons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedClub.persons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    this.searchPersons = this.allPersons;
    document.getElementById('add-person-popup').classList.toggle('hidden', false);
  }

  /**
   * Adds all selected people to the club
   */
  addPersonsToClub() {
    this.selectedAmountOfPersons = 0;
    this.status = this.personServ.updateStatus.subscribe((succ) => {
      this.selectedAmountOfPersons++;
      if (this.selectedAmountOfPersons === this.selectedPersons.length) {
        this.sharedServ.updatePersonsForClub = true;
        this.personServ.getPersons();
        this.status.unsubscribe();
        this.status = null;
        this.loadingAddPersons = false;
        this.disablePopup();
      }
    }, (error) => {

    });
    this.loadingAddPersons = true;
    this.selectedPersons.forEach((person) => {
      this.addPersonToClub(person, this.selectedClub.clubId);
    });
  }

  /**
   * Selects a person that is the selected club
   * @param event
   * @param person
   */
  selectPersonInClub(event, person: any) {
    let selectedItems = document.getElementById('club-person-list');
    for (let i = 0; i < selectedItems.children.length; i++) {
      selectedItems.children[i].classList.toggle('person-item-selected', false);
    }
    event.target.classList.add('person-item-selected');
    this.selectedPerson = person;
  }

  /**
   * Adds a person to a club
   * @param person
   * @param clubId
   */
  addPersonToClub(person, clubId) {
    this.personServ.updatePersonClub(person, clubId);
  }

  /**
   * Sorts clubs by name descending
   */
  filterClubs() {
    this.searchResults = this.searchResults.sort((club1, club2) => {
      if (club1.name < club2.name) return -1;
      else if (club1.name > club2.name) return 1;
      else return 0;
    });
  }

  /**
   * Detects changes in the search person string and filters person search results accordingly
   */
  searchPersonChange() {
    this.searchPersons = [];
    this.resetAllPersons();

    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedClub.persons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedClub.persons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedPersons.length; j++) {
        if ((+this.allPersons[i].personId === +this.selectedPersons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }

    if (this.searchQueryPerson === '') {
      this.searchPersons = this.allPersons;
      return;
    }
    this.allPersons.forEach((person, index) => {
      let name: string = this.name.transform(person);
      if (name.toLowerCase().includes(this.searchQueryPerson.toLowerCase())) {
        this.searchPersons.push({
          firstname: person.firstname,
          prefix: person.prefix,
          lastname: person.lastname,
          personId: person.personId,
          clubId: person.clubId,
          clubName: person.clubName
        })
      }
    });
    this.filterClubs();
  }

  /**
   * Adds a person to the selection when adding people to a club
   * @param person
   */
  addPersonToSelection(person) {
    this.selectedPersons.push({
      firstname: person.firstname,
      lastname: person.lastname,
      prefix: person.prefix,
      personId: person.personId
    });
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedPersons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedPersons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    if (this.searchQueryPerson != '' && this.searchQueryPerson != null && this.searchQueryPerson != undefined) {
      this.searchPersonChange();
    }
  }

  /**
   * Removes a person from the selection when adding people to a club
   * @param person
   */
  removePersonFromSelection(person) {
    this.selectedPersons.forEach((persons, index) => {
      if (+persons.personId === +person.personId) {
        this.selectedPersons.splice(index, 1);
      }
    });
    this.resetAllPersons();
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedClub.persons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedClub.persons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedPersons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedPersons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    this.searchPersons = this.allPersons;
    if (this.searchQueryPerson != '' && this.searchQueryPerson != null && this.searchQueryPerson != undefined) {
      this.searchPersonChange();
    }
  }

  /**
   * Resets all people that can be added to a club
   */
  resetAllPersons() {
    this.allPersons = [];
    this.sharedServ.allPersons.forEach((person) => {
      let clubName: string = null;
      if (this.sharedServ.allClubs.length > 0) {
        if (person.clubId != null && person.clubId != undefined && person.clubId != 'null') {
          clubName = this.sharedServ.allClubs.find((club) => {
            return club.clubId === person.clubId;
          }).name;
        }
      }
      this.allPersons.push({
        firstname: person.firstname,
        prefix: person.prefix,
        lastname: person.lastname,
        personId: person.personId,
        clubName: clubName
      });
    });
  }

  /**
   * Validator that determines wether or not the club can be created according to the inserted data
   * @returns {boolean}
   */
  mayCreateNewClub(): boolean {
    return (this.newClub.name !== '' && this.newClub.name !== undefined && !(new RegExp('^[\\s]+?$').test(this.newClub.name)));
  }

  /**
   * Validator that determines wether or not the club can be edited according to the inserted data
   * @returns {boolean}
   */
  mayEditClub(): boolean {
    return (this.selectedClub.name !== '' && this.selectedClub.name !== undefined && !(new RegExp('^[\\s]+?$').test(this.selectedClub.name)));
  }

  /**
   * Subscribes to ClubService.Clubs
   */
  private subscribeToClubs() {
    this.clubServ.Clubs.subscribe((data) => {
      this.loading = false;
      this.searchResults = this.sharedServ.allClubs;
      if (this.selectedClubEvent !== null) {
        this.selectClub(this.selectedClubEvent, this.selectedClub);
      }
      this.filterClubs();
    }, () => {
      this.loading = false;
      this.filterClubs();
    });
  }
}
