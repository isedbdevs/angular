import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TournamentComponent } from './tournament/tournament.component';
import { MatchComponent } from './match/match.component';
import { IndexComponent } from './index/index.component';
import { PersonComponent } from './person/person.component';
import { MDLDirective } from '../MaterialDesignLiteUpgradeElement';
import { HttpClientModule } from '@angular/common/http';
import { NamePipe } from './name.pipe';
import { FormsModule } from "@angular/forms";
import { ClubComponent } from './club/club.component';
import { ClubService } from "./services/club.service";
import { SharedService } from "./services/shared.service";
import { PersonService } from "./services/person.service";
import { TournamentsComponent } from './tournaments/tournaments.component';
import { TournamentService } from "./services/tournament.service";
import { DatePipe } from "@angular/common";


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TournamentComponent,
    MatchComponent,
    IndexComponent,
    PersonComponent,
    MDLDirective,
    NamePipe,
    ClubComponent,
    TournamentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [SharedService, NamePipe, ClubService, PersonService, TournamentService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}
