import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from "./index/index.component";
import { TournamentComponent } from "./tournament/tournament.component";
import { MatchComponent } from "./match/match.component";
import { PersonComponent } from "./person/person.component";
import { ClubComponent } from "./club/club.component";
import { TournamentsComponent } from "./tournaments/tournaments.component";

const routes: Routes = [
  {path: "", component: IndexComponent},
  {path: "match", component: MatchComponent},
  {path: "tournament/:id", component: TournamentComponent},
  {path: "tournament", component: TournamentComponent},
  {path: "tournaments", component: TournamentsComponent},
  {path: "person", component: PersonComponent},
  {path: "club", component: ClubComponent},
  {path: "**", redirectTo: ""}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
