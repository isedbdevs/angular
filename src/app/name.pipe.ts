import { Pipe, PipeTransform } from '@angular/core';
import { SharedService } from "./services/shared.service";

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {
  constructor(private sharedServ: SharedService) {
  }

  /**
   * Returns a fullname string from a person object
   * @param person person object which needs to have a first and lastname
   * @param unknown flag as unknown when the first and lastname are undefined but personId is defined in the person object
   * @param args
   * @returns {any} string in following format: [lastname], [firstname] ([prefix])
   */
  transform(person: any, unknown?: any, args?: any): any {
    let fullName = person;
    if (unknown === 'unknown') {
      fullName = this.sharedServ.allPersons.find((personFromAll) => {
        return +person.personId === +personFromAll.personId;
      })
    }
    let prefix: string = fullName.prefix;
    let name: string = '';
    name = name + ' ' + fullName.lastname + ', ';
    name = name + fullName.firstname;
    if (prefix !== null || prefix !== '' || !prefix.includes('null') || prefix !== undefined) {
      name = name + ' ' + prefix;
    }
    name = name.replace(' null', ' ');
    return name;
  }

}
