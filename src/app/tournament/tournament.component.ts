import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedService } from "../services/shared.service";
import { ActivatedRoute, Router } from "@angular/router";
import { TournamentService } from "../services/tournament.service";

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss']
})
export class TournamentComponent implements OnInit, OnDestroy {
  /**
   * id that is extracted from the URL
   */
  id: number;

  /**
   * Displays loading bar when true
   * @type {boolean}
   */
  loading: boolean = false;

  /**
   * Object that contains the selected tournament
   */
  tournament: {
    tournamentId?: string,
    tournamentName?: string,
    tournamentDate?: any,
    roundAmount?: string
    persons?: any[]
  };

  /**
   * Contains all users in a tournament and their scores
   * @type {any[]}
   */
  personScores: {
    personId: string,
    score: string,
    firstname?: string,
    lastname?: string,
    prefix?: string,
    clubName?: string
  }[] = [];

  retrievePersonScoresSubscription: any;

  /**
   * Holds subscriptions and destroys them before the component is destroyed
   */
  private sub: any;

  constructor(private route: ActivatedRoute, private router: Router, public sharedServ: SharedService, private tournamentServ: TournamentService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.checkIfUserShouldReturnToTournaments();
    this.subscribeToRetrievePersonScores();
    this.tournamentServ.getTournamentScores(this.tournament.tournamentId);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  /**
   * Checks if there's the selected tournament is undefined and returns user to tournaments if so
   */
  private checkIfUserShouldReturnToTournaments() {
    if (this.sharedServ.selectedTournament === undefined) {
      this.returnToTournaments();
    }
    this.tournament = this.sharedServ.selectedTournament;
    if (this.tournament === undefined) {
      this.returnToTournaments();
    }
    if (this.tournament.tournamentId === undefined || this.id !== +this.tournament.tournamentId) {
      this.returnToTournaments();
    }
  }

  /**
   * Subscribed to retrievePersonScores event
   */
  private subscribeToRetrievePersonScores() {
    this.retrievePersonScoresSubscription = this.tournamentServ.retrievePersonScores.subscribe((bool) => {
      if (bool) {
        this.sharedServ.tournamentPersonScores.forEach((tournament) => {
          if (+tournament.tournamentId === +this.tournament.tournamentId) {
            tournament.scores.forEach((score) => {
              this.personScores.push({
                personId: score.personId,
                score: score.score
              })
            });
            this.personScores.forEach((score) => {
              let person = this.tournament.persons.find((person) => {
                return ('' + person.personId) === (score.personId + '');
              });
              if (person !== undefined) {
                score.clubName = person.clubName;
                score.firstname = person.firstname;
                score.lastname = person.lastname;
                score.prefix = person.prefix;
              }
            });
          }
        });
        this.personScores = this.personScores.sort((object1, object2) => {
          if (+object1.score < +object2.score) return 1;
          else if (+object1.score > +object2.score) return -1;
          else return 0;
        });
        if (this.personScores.length < this.tournament.persons.length) {
          this.tournament.persons.forEach((person) => {
            if (this.personScores.find((personScore) => {
              return personScore.personId === person.personId;
            }) === undefined) {
              this.personScores.push({
                personId: person.personId,
                score: '0',
                firstname: person.firstname,
                lastname: person.lastname,
                prefix: person.prefix,
                clubName: person.clubName
              });
            }
          });
        }
      }
    });
    this.sub.add(this.retrievePersonScoresSubscription);
  }

  /**
   * returns to Tournaments when there's no selected tournament
   */
  private returnToTournaments() {
    this.router.navigateByUrl('/tournaments');
  }

}
