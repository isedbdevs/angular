import { Component, OnInit } from '@angular/core';
import { PersonService } from "./services/person.service";
import { ClubService } from "./services/club.service";
import { TournamentService } from "./services/tournament.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ChessApp';

  constructor(private personServ: PersonService, private clubServ: ClubService, private tournamentServ: TournamentService) {

  }

  ngOnInit() {
    this.clubServ.getClubs();
    this.personServ.getPersons();
    this.tournamentServ.getTournaments();
    this.personServ.getPersonInTournament();
  }
}
