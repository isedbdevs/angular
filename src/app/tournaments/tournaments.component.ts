import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedService } from "../services/shared.service";
import { PersonService } from "../services/person.service";
import { Subscription } from "rxjs/Subscription";
import { NamePipe } from "../name.pipe";
import { TournamentService } from "../services/tournament.service";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.scss']
})
export class TournamentsComponent implements OnInit, OnDestroy {
  /**
   * Object that contains the selected tournaments and all people in that tournament
   */
  selectedTournament: {
    tournamentId?: string,
    tournamentName?: string,
    tournamentDate?: any,
    roundAmount?: string
    persons?: any[]
  };

  /**
   * Displays the create new person form
   * @type {boolean}
   */
  addMode: boolean = false;

  /**
   * displays the edit person form
   * @type {boolean}
   */
  editMode: boolean = false;

  /**
   * Array of all tournaments that are displayed in the tournament list
   * This array is filtered when the user types something in the search field
   * @type {any[]}
   */
  searchResults: {
    tournamentId: string,
    tournamentName: string,
    tournamentDate: Date,
    roundAmount: string
  }[] = [];

  /**
   * object used for creating a new tournament
   * @type {{tournamentName: string; tournamentDate: string; roundAmount: string}}
   */
  newTournament: {
    tournamentName: string,
    tournamentDate: string,
    roundAmount: string
  } = {
    tournamentName: '',
    tournamentDate: '',
    roundAmount: ''
  };

  /**
   * Selected person in a tournament
   */
  selectedPerson: any;
  selectedTournamentEvent: any = null;

  /**
   * Search query which is linked to the search input field
   * @type {string}
   */
  search: string = '';

  /**
   * Shows loading bar if true
   * @type {boolean}
   */
  loading: boolean = false;
  loadingadd: boolean = false;
  loadingedit: boolean = false;

  /**
   * Holds subscriptions
   */
  status: Subscription;

  allPersons: any = [];
  searchPersons: any = [];
  searchQueryPerson: string = '';
  selectedPersons: any = [];
  loadingAddPersons: boolean = false;
  selectedAmountOfPersons: number = 0;

  retrieveTournamentsSubscription: any;
  retrievePersonInTournamentSubscription: any;

  constructor(public sharedServ: SharedService, private personServ: PersonService, private date: DatePipe, private name: NamePipe, private tournamentServ: TournamentService, private router: Router) {

  }

  ngOnInit() {
    this.loading = true;
    this.subscribeToRetrieveTournaments();
    this.subscribeToRetrieveAllPersonInTournament();
    if (this.sharedServ.allTournaments.length > 0) {
      this.searchResults = this.sharedServ.allTournaments;
      this.filterTournaments();
      if (this.loading) {
        this.loading = !this.loading;
      }
    }
  }

  ngOnDestroy() {
    this.retrieveTournamentsSubscription.unsubscribe();
    this.retrievePersonInTournamentSubscription.unsubscribe();
  }

  /**
   * Is executed when a tournament is selected from the tournament list
   * @param event javascript click event
   * @param tournament tournament object
   */
  selectTournament(event: any, tournament: any) {
    this.selectedTournamentEvent = event;
    this.cancelAllModes();
    let selectedItems = document.getElementById('person-list');
    for (let i = 0; i < selectedItems.children.length; i++) {
      selectedItems.children[i].classList.toggle('person-item-selected', false);
    }
    event.target.classList.add('person-item-selected');
    this.selectedTournament = tournament;
    this.selectedTournament.persons = [];
    this.sharedServ.allPersonInTournament.forEach((person) => {
      if (this.selectedTournament.persons.find((selectedPerson) => {
        return +person.personId === +selectedPerson.personId;
      }) === undefined) {
        if (+person.tournamentId === +this.selectedTournament.tournamentId) {
          this.selectedTournament.persons.push({
            personId: person.personId
          });
        }
      }
    });
    this.resetAllPersons();
  }

  /**
   * Show delete tournament popup
   */
  deleteTournament() {
    document.getElementById('delete-popup').classList.toggle('hidden', false);
  }

  /**
   * Hides all popups
   */
  disablePopup() {
    document.getElementById('delete-popup').classList.toggle('hidden', true);
    document.getElementById('add-person-popup').classList.toggle('hidden', true);
  }

  /**
   * Enables add mode
   * Add mode shows the create new person form
   */
  enableAddMode() {
    this.editMode = false;
    this.addMode = true;
    setTimeout(() => {
      document.getElementById('tournamentDate').parentElement.classList.add('is-dirty');
    }, 50);
  }

  /**
   * Enables edit mode
   * Shows the form to edit a person
   */
  enableEditMode() {
    if (this.selectedTournament != null || this.selectedTournament != undefined) {
      this.addMode = false;
      this.editMode = true;
      setTimeout(() => {
        this.isdirty();
      }, 50);
      this.selectedTournament.tournamentDate = this.selectedTournament.tournamentDate.toISOString().substr(0, this.selectedTournament.tournamentDate.toISOString().indexOf('T'));
    }
    else {
      this.deleteTournament();
    }
  }

  /**
   * Hides edit and add forms
   */
  cancelAllModes() {
    this.editMode = false;
    this.addMode = false;
  }

  /**
   * Creates new tournament
   */
  createNewTournament() {
    this.loading = true;
    this.tournamentServ.addTournament(this.newTournament);
  }

  /**
   * Deletes a tournament
   * @param {string} tournamentId Tournament ID of the tournament which will be attempted to be deleted
   */
  confirmDelete(tournamentId: string) {
    this.loading = true;
    this.tournamentServ.deleteTournament(tournamentId);
  }

  /**
   * Executes whenever the search string changes and displays search results accordingly
   */
  searchChange() {
    this.searchResults = [];
    this.sharedServ.allTournaments.forEach((tournament) => {
      let name: string = tournament.tournamentName;
      if (name.toLowerCase().includes(this.search.toLowerCase())) {
        this.searchResults.push(tournament);
      }
    });
    if (this.search === '') {
      this.searchResults = this.sharedServ.allTournaments;
    }
    this.filterTournaments();
  }

  /**
   * Adds CSS classes for MDL input fields when they are in dirty state
   */
  isdirty() {
    if (this.selectedTournament.tournamentName !== null && this.selectedTournament.tournamentName !== undefined) {
      document.getElementById('tournamentName-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedTournament.tournamentDate !== null && this.selectedTournament.tournamentDate !== undefined) {
      document.getElementById('tournamentDate-edit').parentElement.classList.add('is-dirty');
    }
    if (this.selectedTournament.roundAmount !== null && this.selectedTournament.roundAmount !== undefined) {
      document.getElementById('roundAmount-edit').parentElement.classList.add('is-dirty');
    }
  }

  /**
   * Edits tournament
   * @deprecated not allowed to edit tournaments
   */
  updateTournament() {
    this.loadingedit = true;
    if (!(this.status)) {
      this.status = this.tournamentServ.updateStatus.subscribe((success) => {
        if (success === 'success') {
          // TODO: process succesful edit
        }
        this.editMode = false;
        this.addMode = false;
        this.newTournament = {
          tournamentName: '',
          tournamentDate: '',
          roundAmount: ''
        };
        this.resetEdit();
        this.loadingedit = false;
      }, (error) => {
        console.log(error);
        this.resetEdit();
        this.loadingedit = false;
      });
    }
  }

  /**
   * Unsubscribed from the edit event
   * @returns {Promise<void>}
   */
  async resetEdit() {
    setTimeout(() => {
      this.status.unsubscribe();
      this.status = null;
    }, 5);
  }

  /**
   * Deletes a person from a tournament
   * @param {{tournamentId?: string; name?: string; tournamentStreetAddress?: string; tournamentPlace?: string; persons?: {personObject: any}[]}} selectedTournament
   */
  deletePersonFromTournament(selectedTournament: { tournamentId?: string; name?: string; tournamentStreetAddress?: string; tournamentPlace?: string; persons?: { personObject: any }[] }) {
    if (this.selectedPerson !== undefined && this.selectedPerson !== null) {
      this.personServ.deletePersonFromTournament(this.selectedPerson, selectedTournament);
    }
  }

  /**
   * Displays the add person to tournament popup
   */
  openPersonPopup() {
    this.allPersons = [];
    this.searchPersons = [];
    this.searchQueryPerson = '';
    this.selectedPersons = [];
    this.loadingAddPersons = false;
    this.resetAllPersons();
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedTournament.persons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedTournament.persons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    this.searchPersons = this.allPersons;
    document.getElementById('add-person-popup').classList.toggle('hidden', false);
  }

  /**
   * Adds all selected people to the tournament
   */
  addPersonsToTournament() {
    this.selectedAmountOfPersons = 0;
    this.status = this.personServ.updateStatus.subscribe((succ) => {
      this.selectedAmountOfPersons++;
      if (this.selectedAmountOfPersons === this.selectedPersons.length) {
        this.sharedServ.updatePersonsForTournament = true;
        this.personServ.getPersons();
        this.status.unsubscribe();
        this.status = null;
        this.loadingAddPersons = false;
        this.disablePopup();
      }
    }, (error) => {

    });
    this.loadingAddPersons = true;
    this.selectedPersons.forEach((person) => {
      this.addPersonToTournament(person, this.selectedTournament.tournamentId);
    });
  }

  /**
   * Selects a person from the person list of a tournament
   * @param event javascript click event
   * @param person
   */
  selectPersonInTournament(event, person: any) {
    let selectedItems = document.getElementById('tournament-person-list');
    for (let i = 0; i < selectedItems.children.length; i++) {
      selectedItems.children[i].classList.toggle('person-item-selected', false);
    }
    event.target.classList.add('person-item-selected');
    this.selectedPerson = person;
  }

  /**
   * Adds a person to a tournament
   * @param person
   * @param tournamentId
   */
  addPersonToTournament(person, tournamentId) {
    this.personServ.updatePersonTournament(person, tournamentId);
  }

  /**
   * Sorts the tournament list by date
   */
  filterTournaments() {
    this.searchResults = this.searchResults.sort((tournament1, tournament2) => {
      if (tournament1.tournamentDate > tournament2.tournamentDate) return -1;
      else if (tournament1.tournamentDate < tournament2.tournamentDate) return 1;
      else return 0;
    });
  }

  /**
   * Detects changes in the search string and displays search results accordingly
   */
  searchPersonChange() {
    this.searchPersons = [];
    this.resetAllPersons();

    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedTournament.persons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedTournament.persons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedPersons.length; j++) {
        if ((+this.allPersons[i].personId === +this.selectedPersons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }

    if (this.searchQueryPerson === '') {
      this.searchPersons = this.allPersons;
      return;
    }
    this.allPersons.forEach((person, index) => {
      let name: string = this.name.transform(person);
      if (name.toLowerCase().includes(this.searchQueryPerson.toLowerCase())) {
        this.searchPersons.push({
          firstname: person.firstname,
          prefix: person.prefix,
          lastname: person.lastname,
          personId: person.personId,
          tournamentId: person.tournamentId,
          clubName: person.clubName
        })
      }
    });
    this.filterTournaments();
  }

  /**
   * Adds a person to selection when adding people to a tournament
   * @param person
   */
  addPersonToSelection(person) {
    this.selectedPersons.push({
      firstname: person.firstname,
      lastname: person.lastname,
      prefix: person.prefix,
      personId: person.personId
    });
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedPersons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedPersons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    if (this.searchQueryPerson != '' && this.searchQueryPerson != null && this.searchQueryPerson != undefined) {
      this.searchPersonChange();
    }
  }

  /**
   * Removes a person from selection when adding people to a tournament
   * @param person
   */
  removePersonFromSelection(person) {
    this.selectedPersons.forEach((persons, index) => {
      if (+persons.personId === +person.personId) {
        this.selectedPersons.splice(index, 1);
      }
    });
    this.resetAllPersons();
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedTournament.persons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedTournament.persons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    for (let i = this.allPersons.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.selectedPersons.length; j++) {
        if (this.allPersons[i] && (+this.allPersons[i].personId === +this.selectedPersons[j].personId)) {
          this.allPersons.splice(i, 1);
        }
      }
    }
    this.searchPersons = this.allPersons;
    if (this.searchQueryPerson != '' && this.searchQueryPerson != null && this.searchQueryPerson != undefined) {
      this.searchPersonChange();
    }
  }

  /**
   * Resets the list of people that can be added to a tournament
   */
  resetAllPersons() {
    this.allPersons = [];
    this.sharedServ.allPersonInTournament.forEach((person) => {
      let clubName: string = null;
      if (this.sharedServ.allTournaments.length > 0) {
        let fullPerson = this.sharedServ.allPersons.find((tournament) => {
          return tournament.personId === person.personId;
        });
        if (fullPerson.clubId != null && fullPerson.clubId != undefined && fullPerson.clubId != 'null') {
          clubName = this.sharedServ.allClubs.find((club) => {
            return club.clubId === fullPerson.clubId;
          }).name;
        }
      }
      if (this.allPersons.find((arrayPerson) => {
        return arrayPerson.personId === person.personId;
      }) === undefined) {
        this.allPersons.push({
          firstname: this.sharedServ.allPersons.find((tournament) => {
            return tournament.personId === person.personId;
          }).firstname,
          prefix: this.sharedServ.allPersons.find((tournament) => {
            return tournament.personId === person.personId;
          }).prefix,
          lastname: this.sharedServ.allPersons.find((tournament) => {
            return tournament.personId === person.personId;
          }).lastname,
          personId: person.personId,
          clubName: clubName
        });
      }
    });
  }

  /**
   * Validator which returns true when all required data is filled in
   * @returns {boolean}
   */
  mayCreateNewTournament() {
    return ((this.newTournament.tournamentName !== '' && this.newTournament.tournamentName !== undefined && !(new RegExp('^[\\s]+?$').test(this.newTournament.tournamentName))) && (this.newTournament.roundAmount !== '' && this.newTournament.roundAmount !== undefined) && (this.newTournament.tournamentDate !== '' && this.newTournament.tournamentDate !== undefined));
  }

  /**
   * Subscribes to PersonServ.retrieveAllPersonInTournament
   */
  private subscribeToRetrieveAllPersonInTournament() {
    this.retrievePersonInTournamentSubscription = this.personServ.retrieveAllPersonInTournament.subscribe((value) => {
      if (value) {
        if (this.selectedTournament != undefined && this.selectedTournament != null) {
          this.selectTournament(this.selectedTournamentEvent, this.selectedTournament);
          this.filterTournaments();
        }
      }
      if (this.loadingAddPersons) {
        this.loadingAddPersons = !this.loadingAddPersons;
      }
      this.disablePopup();
    });
  }

  /**
   * Subscribes to TournamentServ.retrieveTournaments
   */
  private subscribeToRetrieveTournaments() {
    this.retrieveTournamentsSubscription = this.tournamentServ.retrieveTournaments.subscribe(() => {
      if (this.sharedServ.allTournaments.length > 0) {
        this.searchResults = this.sharedServ.allTournaments;
        this.filterTournaments();
      }
      if (this.loading) {
        this.loading = !this.loading;
      }
    });
  }

  goToTournament() {
    this.sharedServ.selectedTournament = this.selectedTournament;
    this.router.navigateByUrl('/tournament/' + this.selectedTournament.tournamentId);
  }
}
