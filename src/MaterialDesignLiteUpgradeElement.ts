import { AfterViewInit, Directive } from '@angular/core';

declare var componentHandler: any;

@Directive({
  selector: '[appmdl]'
})
export class MDLDirective implements AfterViewInit {
  ngAfterViewInit() {
    componentHandler.upgradeAllRegistered();
  }
}
