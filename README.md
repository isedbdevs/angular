# ChessApp
This project contains the Front-End of the ChessApp by DBDevs

## Pre-requisites
- NodeJS (npm)
- [Angular CLI v1.7.3](https://github.com/angular/angular-cli)
  - run `npm install @angular/cli@1.7.3` to install Angular CLI (add `-g` flag to install it globally)

## Modules
run `npm install` to install all node_modules. This is necessary to run the project.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Deployment

1) Run `ng build --prod --base-href /` where `/` should be the base url of the application.
2) Copy all files within the `dist` folder to the server.
3) Copy `.htaccess` file from project root directory to the directory where the index.html is hosted.
